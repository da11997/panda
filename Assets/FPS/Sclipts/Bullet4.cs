using UnityEngine;
using System.Collections;

public class Bullet4 : MonoBehaviour {

private Vector3 direction;
	// Use this for initialization
	void Start () {
	direction = Camera.mainCamera.transform.forward;
	}
	
	// Update is called once per frame
	void Update () {
	transform.position += direction * 0.08f;
	}
}
