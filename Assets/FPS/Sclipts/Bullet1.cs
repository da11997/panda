using UnityEngine;
using System.Collections;

public class Bullet1 : MonoBehaviour {

private Vector3 direction;
	// Use this for initialization
	void Start () {
	direction = Camera.mainCamera.transform.forward;
	}
	
	// Update is called once per frame
	void Update () {
	transform.position += direction * 0.14f;
	}
}
